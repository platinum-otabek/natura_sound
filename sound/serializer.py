from rest_framework.serializers import ModelSerializer
from .models import MusicModel, GroupModels
from rest_framework import serializers


class GroupSerializer(ModelSerializer):
    class Meta:
        model = GroupModels
        fields = ('name', 'main_group', 'slug')


class MusicSerializer(ModelSerializer):
    file_field = serializers.SerializerMethodField()

    class Meta:
        model = MusicModel
        fields = ('name', 'hz', 'file_field', 'arabic_name','group_field', 'slug')

    def get_file_field(self, obj):
        if obj.file_field:
            request = self.context['request']
            return request.build_absolute_uri(obj.file_field.url)
        return None
