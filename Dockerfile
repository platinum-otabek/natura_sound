# Use an official Python runtime as a parent image
FROM python:3.10-slim as builder

# Set environment variables for Python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# Create and set the working directory
WORKDIR /app

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
# Install system dependencies
#RUN apt-get update && \
#    apt-get install -y --no-install-recommends gcc libpq-dev && \
#    rm -rf /var/lib/apt/lists/*

# Install Python dependencies
COPY ./requirements.txt ./requirements.txt

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install uvicorn

# ---- Second stage ----
FROM python:3.10-slim

WORKDIR /app

RUN apt update && apt install -y libpq-dev gettext


ENV PATH="/opt/venv/bin:$PATH"

# Copy only necessary files from the builder stage
COPY --from=builder /opt/venv /opt/venv
COPY . .

CMD python manage.py collectstatic --noinput && \
    python manage.py migrate &&
# Expose the port that Gunicorn will run on

EXPOSE 8001

CMD python manage.py collectstatic --noinput && \
    python manage.py migrate && \
    uvicorn config.asgi:application --host 0.0.0.0 --port 8001
