from django.db import models
from django.utils.crypto import get_random_string
from config.util import unique_slug_generator
from django.db.models.signals import pre_save


# Create your models here.

class GroupModels(models.Model):
    name = models.CharField(max_length=15, default='')
    main_group = models.ForeignKey('GroupModels', on_delete=models.CASCADE, null=True)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class MusicModel(models.Model):
    name = models.CharField(max_length=255, default='')
    arabic_name = models.CharField(max_length=255, default='')
    hz = models.PositiveSmallIntegerField(default=1)
    file_field = models.FileField(upload_to='music/')
    slug = models.SlugField(unique=True)
    group_field = models.ForeignKey(GroupModels, on_delete=models.CASCADE, default=None)


def slug_generator(sender, instance, *args, **kwargs):
    if not instance.slug:
        if instance.name:
            name = instance.name_uz
        elif instance.title_uz:
            name = instance.name_uz
        elif instance.title_ru:
            name = instance.name_ru
        else:
            name = get_random_string(8, '0123456789')

        instance.slug = unique_slug_generator(instance, title=name)


pre_save.connect(slug_generator, sender=GroupModels)
pre_save.connect(slug_generator, sender=MusicModel)
