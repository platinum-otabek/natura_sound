from django.urls import path
from .views import ListGroupApiView,ListMusicApiView

urlpatterns = [
    path('list_groups/', ListGroupApiView.as_view()),
    path('list_music/<slug>', ListMusicApiView.as_view()),
]
