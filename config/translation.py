from modeltranslation.translator import translator, TranslationOptions
from sound.models import GroupModels, MusicModel


# @translator.register(GroupModels)
class GroupModelTranslationOptions(TranslationOptions):
    fields = ('name',)


# @translator.register(MusicModel)
class MusicModelTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(GroupModels, GroupModelTranslationOptions)
translator.register(MusicModel, MusicModelTranslationOptions)
