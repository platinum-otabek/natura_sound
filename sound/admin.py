from django.contrib import admin
from .models import GroupModels, MusicModel
from .forms import GroupModelsForm,MusicModelForm


# Register your models here.


class GroupModelsAdmin(admin.ModelAdmin):
    list_display = ['name', 'main_group']
    form = GroupModelsForm


class MusicModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'group_field', 'file_field']
    form = MusicModelForm


admin.site.register(GroupModels, GroupModelsAdmin)
admin.site.register(MusicModel, MusicModelAdmin)
