from django.shortcuts import render
from rest_framework.generics import ListAPIView
from .models import GroupModels, MusicModel
from .serializer import GroupSerializer, MusicSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404


class ListGroupApiView(ListAPIView):
    queryset = GroupModels.objects.all()
    serializer_class = GroupSerializer


class ListMusicApiView(APIView):
    def get(self, request, *args, **kwargs):
        slug = self.kwargs.get('slug')
        group_instance = get_object_or_404(GroupModels, slug=slug)

        musics = MusicModel.objects.filter(group_field=group_instance)
        serializer = MusicSerializer(musics, many=True,context={'request':self.request})
        return Response(serializer.data)
