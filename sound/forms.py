from django import forms
from .models import GroupModels, MusicModel


class GroupModelsForm(forms.ModelForm):
    name_uz = forms.CharField(label='Name uz', widget=forms.TextInput({'class': 'form-control'}), required=True)
    name_en = forms.CharField(label='Name en', widget=forms.TextInput({'class': 'form-control'}), required=False)
    name_ru = forms.CharField(label='Name ru', widget=forms.TextInput({'class': 'form-control'}), required=False)

    class Meta:
        model = GroupModels
        # fields = ['name', 'main_group']
        exclude = ['name', 'slug']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['main_group'] = forms.ModelChoiceField(
            queryset=GroupModels.objects.all(),  # Adjust the queryset as needed
            required=False
        )
        # Set main_group field as not required
        self.fields['main_group'].required = False


class MusicModelForm(forms.ModelForm):
    name_uz = forms.CharField(label='Name uz', widget=forms.TextInput({'class': 'form-control'}), required=True)
    name_en = forms.CharField(label='Name en', widget=forms.TextInput({'class': 'form-control'}), required=False)
    name_ru = forms.CharField(label='Name ru', widget=forms.TextInput({'class': 'form-control'}), required=False)

    arabic_name = forms.CharField(label='Arabic name', widget=forms.TextInput({'class': 'form-control'}), required=False)

    class Meta:
        model = MusicModel
        exclude = ['name', 'slug']
